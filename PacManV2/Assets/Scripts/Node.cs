﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

	public Node[] neighs;
	public Vector2[] dirValidas;

	// Use this for initialization
	void Start () {
		dirValidas = new Vector2[neighs.Length];

		for (int i = 0; i < neighs.Length; i++) {
			Node n = neighs [i];
			Vector2 temp = n.transform.localPosition - transform.localPosition;
			dirValidas [i] = temp.normalized;
		}

	}

}
