﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Board : MonoBehaviour {

	private static int WIDTH = 1000;
	private static int HEIGHT = 1000;

	private bool siEmpezoDeath = false;
	private bool siEmpezoComido = false;

	private int playerLevel = 1;

	public GameObject[,] board = new GameObject[WIDTH,HEIGHT];

	public int totBolitas = 0;
	public int score = 0;
	public int vidas = 3;

	public int pelletsComidos = 0;

	public bool isN1;
	public bool isN12;
	public bool isN13;
	public bool isN2;
	public bool isN22;
	public bool isN23;
	public bool isN3;
	public bool isN32;
	public bool isN33;
	public bool isN4;
	public bool isN42;
	public bool isN43;


	public Text playtxt;
	public Text rdytxt;
	public Text highScoreText;
	public Text scoreText;
	public Image playerLives2;
	public Image playerLives3;
	public Text ghostComidoScoreText;
	public Text levelNum;
	public Image playerLives1Ext;


	public AudioClip bgNormalAudio;
	public AudioClip bgScaredAudio;
	public AudioClip bgDeathAudio;
	public AudioClip ghostComidoAudio;




	// Use this for initialization
	void Start () {

		Object[] objs = GameObject.FindObjectsOfType (typeof(GameObject));

		foreach (GameObject o in objs) {
			Vector2 pos = o.transform.position;
			if (o.name != "Pacman"  && o.name != "Maze" && o.name != "Bolitas"&& o.tag != "Ghost" && o.name != "Nodos" && o.name != "NotNodos" && o.tag != "ghostHome" && o.name != "Canvas" && o.tag != "UIElements") {
				if (o.GetComponent<Tile> () != null) {
					if (o.GetComponent<Tile> ().isPellet == true || o.GetComponent<Tile> ().isEnePellet == true) {
						totBolitas++;
					}
				}
				board [(int)pos.x, (int)pos.y] = o;
			} 
		}

		Empezar ();
	}

	void Update(){
		UpdateUI ();
		checkBolitasComidas ();
	}

	void UpdateUI(){
		scoreText.text = score.ToString ();
		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");

		if (pacman.transform.GetComponent<PacManMovement> ().tieneExtra == false) {
			if (vidas == 3) {
				playerLives3.enabled = true;
				playerLives2.enabled = true;

			} else if (vidas == 2) {
				playerLives3.enabled = false;
				playerLives2.enabled = true;
			} else if (vidas == 1) {
				playerLives3.enabled = false;
				playerLives2.enabled = false;
			}
		} else {
			if (vidas == 4) {
				playerLives3.enabled = true;
				playerLives2.enabled = true;
				playerLives1Ext.enabled = true;
			} else if (vidas == 3) {
				playerLives3.enabled = false;
				playerLives2.enabled = true;
				playerLives1Ext.enabled = true;

			} else if (vidas == 2) {
				playerLives3.enabled = false;
				playerLives2.enabled = false;
				playerLives1Ext.enabled = true;
			} else if (vidas == 1) {
				playerLives3.enabled = false;
				playerLives2.enabled = false;
				playerLives1Ext.enabled = false;
			}

		}
	}

	void checkBolitasComidas(){
		if (totBolitas == pelletsComidos) {
			ganar ();
		}
	}

	void ganar(){
		playerLevel++;
		StartCoroutine (processGanar (2));
	}

	IEnumerator processGanar(float delay){
		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<PacManMovement> ().canMove = false;
		pacman.transform.GetComponent<Animator> ().enabled = false;
		pacman.transform.GetComponent<AudioSource> ().Stop ();

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<Ghost> ().canMove = false;
			ghost.transform.GetComponent<Animator> ().enabled = false;
		}

		transform.GetComponent<AudioSource> ().Stop ();

		yield return new WaitForSeconds (delay);

		StartCoroutine (esconderGYP (2));

	}

	IEnumerator esconderGYP(float delay){
		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<SpriteRenderer> ().enabled = false;

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<SpriteRenderer> ().enabled = false;
		}

		yield return new WaitForSeconds (delay);
		empezarNextNivel ();

	}

	void empezarNextNivel(){

		if (isN1) {
			SceneManager.LoadScene ("N1(2)");
		} else if (isN12) {
			SceneManager.LoadScene ("N1(3)");
		} else if (isN13) {
			SceneManager.LoadScene ("N2");
		} else if (isN2) {
			SceneManager.LoadScene ("N2(2)"); 
		} else if (isN22) {
			SceneManager.LoadScene ("N2(3)"); 
		} else if (isN23) {
			SceneManager.LoadScene ("N3"); 
		}else if (isN3) {
			SceneManager.LoadScene ("N3(2)"); 
		} else if (isN32) {
			SceneManager.LoadScene ("N3(3)"); 
		} else if (isN33) {
			SceneManager.LoadScene ("N4"); 
		} else if (isN4) {
			SceneManager.LoadScene ("N4(2)"); 
		} else if (isN42) {
			SceneManager.LoadScene ("N4(3)"); 
		} else if (isN43) {
			SceneManager.LoadScene ("FinalScene"); 
		} 
		//SOlo resetea el nivel, tendriamos que ir cambiando estooo
		//podria hacer una variable bool con el nombre de cada uno de los niveles y ponerlos todos ahi para mandarselos aqui

	}

	public void Empezar(){
		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<SpriteRenderer> ().enabled = false;
			ghost.transform.GetComponent<Ghost> ().canMove = false;
		}

		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<SpriteRenderer> ().enabled = false;
		pacman.transform.GetComponent<PacManMovement> ().canMove = false;

		StartCoroutine (enableDespues (2.25f));
		
	}

	public void empezarComido(Ghost gComido){
		if (!siEmpezoComido) {
			siEmpezoComido = true;
			GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
			foreach (GameObject ghost in g) {
				ghost.transform.GetComponent<Ghost> ().canMove = false;
			}
			GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
			pacman.transform.GetComponent<PacManMovement> ().canMove = false;
			pacman.transform.GetComponent<SpriteRenderer> ().enabled = false;
			gComido.transform.GetComponent<SpriteRenderer> ().enabled = false;
			transform.GetComponent<AudioSource> ().Stop ();
			Vector2 p = gComido.transform.position;
			Vector2 vp = Camera.main.WorldToViewportPoint (p); //coordenadas del ghost pq el canvas es un elemento diferente al juego.

			ghostComidoScoreText.GetComponent<RectTransform>().anchorMin = vp;
			ghostComidoScoreText.GetComponent<RectTransform>().anchorMax = vp;
			ghostComidoScoreText.GetComponent<Text> ().enabled = true;

			transform.GetComponent<AudioSource> ().PlayOneShot (ghostComidoAudio);
			StartCoroutine (processComidoDespues (0.75f, gComido));


		}
	}

	IEnumerator processComidoDespues(float delay, Ghost gComido){
		yield return new WaitForSeconds (delay);
		ghostComidoScoreText.GetComponent<Text> ().enabled = false;

		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<SpriteRenderer> ().enabled = true;

		gComido.transform.GetComponent<SpriteRenderer> ().enabled = true;

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<Ghost> ().canMove = true;
		}

		pacman.transform.GetComponent<PacManMovement> ().canMove = true;
		transform.GetComponent<AudioSource> ().Play ();
		siEmpezoComido = false;
	}

	IEnumerator enableDespues(float delay){
		yield return new WaitForSeconds (delay);

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<SpriteRenderer> ().enabled = true;
		}

		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<SpriteRenderer> ().enabled = true;

		playtxt.transform.GetComponent<Text> ().enabled = false;

		StartCoroutine (startGameDespues (2));

	}

	IEnumerator startGameDespues(float delay){
		yield return new WaitForSeconds (delay);

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<Ghost> ().canMove = true;
		}

		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<PacManMovement> ().canMove = true;

		rdytxt.transform.GetComponent<Text> ().enabled = false;
		transform.GetComponent<AudioSource> ().clip = bgNormalAudio;
		transform.GetComponent<AudioSource> ().Play ();
	}

	public void startDeath(){
		if (!siEmpezoDeath) {
			StopAllCoroutines ();
			siEmpezoDeath = true;
			GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
			foreach (GameObject ghost in g) {
				ghost.transform.GetComponent<Ghost> ().canMove = false;
			}

			GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
			pacman.transform.GetComponent<PacManMovement> ().canMove = false;
			pacman.transform.GetComponent<Animator> ().enabled = false;
			transform.GetComponent<AudioSource> ().Stop ();
			StartCoroutine (processDeathAfter (2));
		}
		
	}

	IEnumerator processDeathAfter(float delay){
		yield return new WaitForSeconds (delay);

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<SpriteRenderer> ().enabled = false;
		}

		StartCoroutine (processDeathAnimacion (1.9f));

	}

	IEnumerator processDeathAnimacion(float delay){
		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.localScale = new Vector3 (1, 1, 1);
		pacman.transform.localRotation = Quaternion.Euler (0, 0, 0);
		pacman.transform.GetComponent<Animator> ().runtimeAnimatorController = pacman.transform.GetComponent<PacManMovement> ().deathAni;
		pacman.transform.GetComponent<Animator> ().enabled = true;

		transform.GetComponent<AudioSource> ().clip = bgDeathAudio;
		transform.GetComponent<AudioSource> ().Play ();

		yield return new WaitForSeconds (delay);

		StartCoroutine (processRestart (2));

	}

	IEnumerator processRestart(float delay){
		
		vidas -= 1;

		if (vidas == 0) {
			playtxt.transform.GetComponent<Text> ().enabled = true;
			rdytxt.transform.GetComponent<Text> ().text = "GAME OVER";
			rdytxt.transform.GetComponent<Text> ().color = Color.red;
			rdytxt.transform.GetComponent<Text> ().enabled = true;

			GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
			pacman.transform.GetComponent<SpriteRenderer> ().enabled = false;
			transform.GetComponent<AudioSource> ().Stop ();
			StartCoroutine (processGameOver (2));
			
			
		} else {

			playtxt.transform.GetComponent<Text> ().enabled = true;
			rdytxt.transform.GetComponent<Text> ().enabled = true;

			GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
			pacman.transform.GetComponent<SpriteRenderer> ().enabled = false;
			transform.GetComponent<AudioSource> ().Stop ();

			yield return new WaitForSeconds (delay);

			StartCoroutine (restartShowCosas (1));
		}
	}

	IEnumerator processGameOver(float delay){
		yield return new WaitForSeconds (delay);
		SceneManager.LoadScene ("MainMenu");
	}

	IEnumerator restartShowCosas(float delay){

		playtxt.transform.GetComponent<Text> ().enabled = false;
		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<SpriteRenderer> ().enabled = true;
			ghost.transform.GetComponent<Ghost> ().moverAPosIni ();
		}

		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<Animator> ().enabled = false;
		pacman.transform.GetComponent<SpriteRenderer> ().enabled = true;
		pacman.transform.GetComponent<PacManMovement> ().moverAPosIni ();


		yield return new WaitForSeconds (delay);

		Restart ();
		
	}

	public void Restart(){
		rdytxt.transform.GetComponent<Text> ().enabled = false;

		siEmpezoDeath = false;
		GameObject pacman = GameObject.FindGameObjectWithTag ("Pacman");
		pacman.transform.GetComponent<PacManMovement> ().Restart();

		GameObject[] g = GameObject.FindGameObjectsWithTag ("Ghost");
		foreach (GameObject ghost in g) {
			ghost.transform.GetComponent<Ghost> ().Restart ();
		}

		transform.GetComponent<AudioSource> ().clip = bgNormalAudio;
		transform.GetComponent<AudioSource> ().Play ();


	}

		
}
