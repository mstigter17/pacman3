﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	public bool isPortal;
	public bool isPellet;
	public bool isEnePellet;
	public bool didConsume;
	public bool isEntradaHouse;
	public bool isHouse;
	public bool isInvisiblePowerUp;
	public bool isFastPowerUp;
	public bool isGhostSlowPowerUp;
	public bool isExtraLifePowerUp;

	public GameObject portalReciever;
}
