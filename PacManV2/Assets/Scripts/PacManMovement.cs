﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PacManMovement : MonoBehaviour {

	public float speed = 4.0f;
	public float normalSpeed = 4.0f;
	public float fastSpeed = 8.0f;
	public Vector2 orient;
	private Vector2 dir = Vector2.zero;
	private Vector2 sigDir;

	private Node current,targetN, antN;
	private Node posIni;

	public Sprite idle;
	public Sprite idleInvisible;

	private bool playedSonido1 = false;
	public AudioClip sonido1;
	public AudioClip sonido2;
	public AudioClip pwrup;
	public AudioSource audio;


	public Text invModeText;
	public Text fastModeText;
	public Text slowgText;

	public RuntimeAnimatorController chompAni;
	public RuntimeAnimatorController deathAni;
	public RuntimeAnimatorController invisibleAni;

	public bool canMove = true;
	public bool canBeEaten = true;
	public bool tieneExtra = false;

	public int startNormalAt = 7;
	private float invisibleModeTimer = 0;
	private float invisibleModeDuracion = 15;
	private float normalBlinker = 0;
	private bool invModeEsNormal = false;

	private float fastModeTimer = 0;
	private float fastModeDuracion = 10;
	private bool fastModeEsNormal = false;

	public enum PowerUp{
		None, Invisible, Fast, ExtraLife
	}

	PowerUp currentPower = PowerUp.None;


	// Use this for initialization
	void Start () {

		audio = transform.GetComponent<AudioSource> ();
		Node n = getNodeEnPos (transform.localPosition);
		posIni = n;

		if (n != null) {
			current = n;
		}

		dir = Vector2.left;
		orient = Vector2.left;
		cambiarPos (dir);
	}

	public void Restart(){
		canMove = true;
		current = posIni;

		sigDir = Vector2.left;

		transform.GetComponent<Animator> ().runtimeAnimatorController = chompAni;
		transform.GetComponent<Animator> ().enabled = true;

		cambiarPos (dir);
	
	}

	public void moverAPosIni(){

		transform.position = posIni.transform.position;

		transform.GetComponent<SpriteRenderer> ().sprite = idle;

		dir = Vector2.left;
		orient = Vector2.left;
		orientacion ();

	}
	
	// Update is called once per frame
	void Update () {
		if (canMove) {
			updatePowerUp ();
			CheckInputUser ();
			moverse ();
			orientacion ();
			estadoAnimacion ();
			comer ();
		}
	}

	void playSonidoComiendo(){
		if (playedSonido1) {
			audio.PlayOneShot (sonido2);
			playedSonido1 = false;
		} else {
			audio.PlayOneShot (sonido1);
			playedSonido1 = true;
		}
	}
		
	void CheckInputUser(){

		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			cambiarPos (Vector2.left);
		}
		else if (Input.GetKeyDown (KeyCode.RightArrow)){
			cambiarPos (Vector2.right);
		} 
		else if(Input.GetKeyDown(KeyCode.UpArrow)){
			cambiarPos (Vector2.up);
		} 
		else if(Input.GetKeyDown(KeyCode.DownArrow)){
			cambiarPos (Vector2.down);
		}

	}

	void orientacion(){
		if (dir == Vector2.left) {
			orient = Vector2.left;
			transform.localScale = new Vector3 ((int)-1,(int)1,(int)1);
			transform.localRotation = Quaternion.Euler (0, 0, 0);
		}
		else if (dir == Vector2.right) {
			orient = Vector2.right;
			transform.localScale = new Vector3 ((int)1,(int)1,(int)1);
			transform.localRotation = Quaternion.Euler (0, 0, 0);
		}
		else if (dir == Vector2.up) {
			orient = Vector2.up;
			transform.localScale = new Vector3 ((int)1,(int)1,(int)1);
			transform.localRotation = Quaternion.Euler (0, 0, 90);
		}
		else if (dir == Vector2.down) {
			orient = Vector2.down;
			transform.localScale = new Vector3 ((int)1,(int)1,(int)1);
			transform.localRotation = Quaternion.Euler (0, 0, 270);
		}
	}

	void estadoAnimacion(){

		if (currentPower == PowerUp.None) {
			transform.GetComponent<Animator> ().runtimeAnimatorController = chompAni;
			speed = normalSpeed;
			if (dir == Vector2.zero) {
				GetComponent<Animator> ().enabled = false;
				GetComponent<SpriteRenderer> ().sprite = idle;
			} else {
				GetComponent<Animator> ().enabled = true;
			}
		} else if (currentPower == PowerUp.Invisible) {
			transform.GetComponent<Animator> ().runtimeAnimatorController = invisibleAni;
			if (dir == Vector2.zero) {
				GetComponent<Animator> ().enabled = false;
				GetComponent<SpriteRenderer> ().sprite = idleInvisible;
			} else {
				GetComponent<Animator> ().enabled = true;
			}
		} if (currentPower == PowerUp.Fast) {
			transform.GetComponent<Animator> ().runtimeAnimatorController = chompAni;
			speed = fastSpeed;
			if (dir == Vector2.zero) {
				GetComponent<Animator> ().enabled = false;
				GetComponent<SpriteRenderer> ().sprite = idle;
			} else {
				GetComponent<Animator> ().enabled = true;
			}
		}
			
	}

	void moverse(){
		if (targetN != current && targetN != null) {
			if (sigDir == dir * -1) {
				dir *= -1;
				Node temp = targetN;
				targetN = antN;
				antN = temp;
			}
			if (overST ()) {
				current = targetN;
				transform.localPosition = current.transform.position;
				GameObject otroP = getPortal (current.transform.position);
				if (otroP != null) {
					transform.localPosition = otroP.transform.position;
					current = otroP.GetComponent<Node> ();
				}

				Node moverseA = puedeMover (sigDir);
				if (moverseA != null) {
					dir = sigDir;
				}
				if (moverseA == null) {
					moverseA = puedeMover (dir);
				}
				if (moverseA != null) {
					targetN = moverseA;
					antN = current;
					current = null;
				} else {
					dir = Vector2.zero;
				}

			} else {
				transform.localPosition += (Vector3)(dir * speed) * Time.deltaTime;
			}

		}
			
	}

	void moverANode(Vector2 x){
		Node temp = puedeMover (x);

		if (temp != null) {
			transform.position = temp.transform.position;
			current = temp;
		}
	}

	Node puedeMover(Vector2 x){
		Node temp = null;

		for (int i = 0; i < current.neighs.Length; i++) {
			if (current.dirValidas[i] == x) {
				temp = current.neighs[i];
				break;
			}
		}
		return temp;
	}

	Node getNodeEnPos(Vector2 pos){
		GameObject t = GameObject.Find ("Juego").GetComponent<Board> ().board [(int)pos.x, (int)pos.y];
		if (t != null) {
			if (t.GetComponent<Node> () != null) {
				return t.GetComponent<Node> ();
			}

		}
		return null;
	}

	void cambiarPos(Vector2 x){
		if (x != dir) {
			sigDir = x;
		}

		if (current != null) {
			Node moverAN = puedeMover (x);

			if (moverAN != null) {
				dir = x;
				targetN = moverAN;
				antN = current;
				current = null;
			}
		}
	}

	float lenDeNodo(Vector2 tp){
		Vector2 v = tp - (Vector2)antN.transform.position;
		return v.sqrMagnitude;
	}

	bool overST(){
		float nodoAT = lenDeNodo (targetN.transform.position);
		float nodoAMismo = lenDeNodo (transform.localPosition);

		return nodoAMismo > nodoAT;
	}



	GameObject getPortal(Vector2 x){
		GameObject t = GameObject.Find("Juego").GetComponent<Board> ().board [(int)x.x, (int)x.y];

		if (t != null) {
			if (t.GetComponent<Tile> () != null) {
				if (t.GetComponent<Tile> ().isPortal) {
					GameObject otroP = t.GetComponent<Tile> ().portalReciever;
					return otroP;
				}
			}

		}

		return null;
	}

	void comer(){
		GameObject obj = getTileEnPos (transform.position);

		if (obj != null) {
			Tile t = obj.GetComponent<Tile> ();

			if (t != null) {
				if (!t.didConsume && (t.isPellet || t.isEnePellet)) {
					obj.GetComponent<SpriteRenderer> ().enabled = false;
					t.didConsume = true;
					GameObject.Find ("Juego").transform.GetComponent<Board> ().score += 10;
					GameObject.Find ("Juego").transform.GetComponent<Board> ().pelletsComidos++;
					playSonidoComiendo ();
					if (t.isEnePellet) {
						GameObject[] fanstasmas = GameObject.FindGameObjectsWithTag ("Ghost");
						foreach (GameObject g in fanstasmas) {
							g.GetComponent<Ghost> ().startScaredModo ();
						}
					}
				} else if (!t.didConsume && t.isInvisiblePowerUp) {
					t.didConsume = true;
					canBeEaten = false;
					audio.PlayOneShot (pwrup);
					obj.GetComponent<SpriteRenderer> ().enabled = false;
					invModeText.GetComponent<Text> ().enabled = true;
					startInvisiblePower ();
				} else if (!t.didConsume && t.isFastPowerUp) {
					t.didConsume = true;
					audio.PlayOneShot (pwrup);
					obj.GetComponent<SpriteRenderer> ().enabled = false;
					fastModeText.GetComponent<Text> ().enabled = true;
					startFastPower ();
				} else if (!t.didConsume && t.isGhostSlowPowerUp == true) {
					t.didConsume = true;
					audio.PlayOneShot (pwrup);
					obj.GetComponent<SpriteRenderer> ().enabled = false;
					slowgText.GetComponent<Text> ().enabled = true;
					GameObject[] fanstasmas = GameObject.FindGameObjectsWithTag ("Ghost");
					foreach (GameObject g in fanstasmas) {
						g.GetComponent<Ghost> ().startSlowModo ();
					}
				} else if (!t.didConsume && t.isExtraLifePowerUp == true) {
					t.didConsume = true;
					audio.PlayOneShot (pwrup);
					obj.GetComponent<SpriteRenderer> ().enabled = false;
					tieneExtra = true;
					GameObject.Find ("Juego").transform.GetComponent<Board> ().vidas++;
				}
			}
		}
	}

	void updatePowerUp(){
		if (currentPower == PowerUp.Invisible) {
			invisibleModeTimer += Time.deltaTime;
			if (invisibleModeTimer >= invisibleModeDuracion) {
				invModeText.GetComponent<Text> ().enabled = false;
				audio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgNormalAudio;
				audio.Play ();
				canBeEaten = true;
				invisibleModeTimer = 0;
				cambiarPower (PowerUp.None);
			}
			if (invisibleModeTimer >= startNormalAt) {
				normalBlinker += Time.deltaTime;

				if (normalBlinker >= 0.1f) {
					normalBlinker = 0;
					if (invModeEsNormal) {
						transform.GetComponent<Animator> ().runtimeAnimatorController = invisibleAni;
						invModeEsNormal = false;
					} else {
						transform.GetComponent<Animator> ().runtimeAnimatorController = chompAni;
						invModeEsNormal = true;
					}
				}
			}
		} else if (currentPower == PowerUp.Fast) {
			fastModeTimer += Time.deltaTime;
			if (fastModeTimer >= fastModeDuracion) {
				fastModeText.GetComponent<Text> ().enabled = false;
				audio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgNormalAudio;
				audio.Play ();
				fastModeTimer = 0;
				cambiarPower (PowerUp.None);
			}
			if (fastModeTimer >= startNormalAt) {
				normalBlinker += Time.deltaTime;

				if (normalBlinker >= 0.1f) {
					normalBlinker = 0;
					if (fastModeEsNormal) {
						speed = fastSpeed;
						fastModeEsNormal = false;
					} else {
						speed = normalSpeed;
						fastModeEsNormal = true;
					}
				}
			}
		}
	}

	void startInvisiblePower(){
		invisibleModeTimer = 0;
		audio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgScaredAudio;
		audio.Play ();
		cambiarPower (PowerUp.Invisible);
	}

	void startFastPower(){
		fastModeTimer = 0;
		audio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgScaredAudio;
		audio.Play ();
		cambiarPower (PowerUp.Fast);
	}

	void cambiarPower(PowerUp p){
		currentPower = p;
		estadoAnimacion ();
	}

	GameObject getTileEnPos(Vector2 x){

		GameObject t = GameObject.Find("Juego").GetComponent<Board> ().board [(int)x.x, (int)x.y];

		if (t != null) {
			return t;
		}

		return null;
	}


}
