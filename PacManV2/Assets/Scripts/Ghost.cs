﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour {

	public float speedMovimiento = 3.9f;
	public float scaredModoSpeed = 2.9f;
	public float comidoSpeed = 15f;
	public float normalSpeed = 3.9f;
	public float slowSpeed = 1.9f;

	public bool canMove = true;

	public Node posIni;
	public Node homeNodo;
	public Node house;

	public int pinkReleaseTimer = 5;
	public int blueReleaseTimer = 14;
	public int orangeReleaseTimer = 21;

	public int scaredModoDuracion = 10;
	public int startWhiteAt = 7;

	public float ghostReleaseTimer = 0;
	public bool isInHouse = false;


	public int locoGhostTimer1 = 7;
	public int chasingMode1 = 20;
	public int locoGhostTimer2 = 7;
	public int chasingMode2 = 20;
	public int locoGhostTimer3 = 5;
	public int chasingMode3 = 20;
	public int locoGhostTimer4 = 5;
	public int chasingMode4 = 20;

	public RuntimeAnimatorController ghostArriba; //para que sea durante el runtime del juego
	public RuntimeAnimatorController ghostAbajo;
	public RuntimeAnimatorController ghostIzq;
	public RuntimeAnimatorController ghostDer;
	public RuntimeAnimatorController ghostBlanco;
	public RuntimeAnimatorController ghostScared;

	public Sprite ojosUp;
	public Sprite ojosAbajo;
	public Sprite ojosIzq;
	public Sprite ojosDer;

	private int iteradorCambioMode = 1;
	private float timerCambioMode = 0;
	private float previoSpeedMover;
	private float scaredModoTimer = 0;
	private float whiteBlinkTimer = 0;

	public int startNormalAt = 7;
	private float slowModeTimer = 0;
	private float slowModeDuracion = 10;
	private float normalBlinker = 0;
	private bool slowModeEsNormal = false;

	private AudioSource bgAudio;

	private bool scaredModoEsBlanco = false;

	public enum Mode{
		Chase,Loco,Scared,Comido, Slow
	}

	public enum GhostTipo{
		Red, Pink, Blue, Orange
	}

	public GhostTipo tipoGhost;

	Mode current = Mode.Loco;
	Mode anterior; 

	private GameObject pacman;

	private Node currNode, targetNode, antNode;
	private Vector2 dir, sigDir;

	// Use this for initialization
	void Start () {

		bgAudio = GameObject.Find ("Juego").transform.GetComponent<AudioSource> ();

		pacman = GameObject.FindGameObjectWithTag("Pacman");
		Node n = getNodoEnPos (transform.localPosition);
		if (n != null) {
			currNode = n;
		}

		if (isInHouse) {
			dir = Vector2.up;
			targetNode = currNode.neighs [0];
		} else {
			dir = Vector2.left;
			targetNode = escogerSiguienteNodo ();
		}
			
		antNode = currNode;
		updateAnimacion ();
	}

	public void Restart(){
		
		canMove = true;

		current = Mode.Loco;
		speedMovimiento = normalSpeed;
		previoSpeedMover = 0;

		ghostReleaseTimer = 0;
		iteradorCambioMode = 1;
		timerCambioMode = 0;

		currNode = posIni;
		if (isInHouse) {
			dir = Vector2.up;
			targetNode = currNode.neighs [0];
		} else {
			dir = Vector2.left;
			targetNode= escogerSiguienteNodo();
		}

		antNode = currNode;


	}

	public void moverAPosIni(){
		transform.position = posIni.transform.position;

		if (isInHouse) {
			dir = Vector2.up;
		} else {
			dir = Vector2.left;
		}

		if (transform.name.Equals ("RedGhost")) {
			isInHouse = false;
		} else {
			isInHouse = true;
		}

		updateAnimacion ();
		
	}

	// Update is called once per frame
	void Update () {
		if (canMove && pacman.transform.GetComponent<PacManMovement> ().canBeEaten == true) {
			updateModo ();
			moverGhost ();
			releaseGhosts ();
			checkIfInHouse ();
			checkCollision ();
		} else if (canMove && pacman.transform.GetComponent<PacManMovement> ().canBeEaten == false) {
			updateModo ();
			moverGhost ();
			releaseGhosts ();
			checkIfInHouse ();
		}
	}

	void checkIfInHouse(){
		if (current == Mode.Comido) {
			GameObject t = getTileEnPos (transform.position);
			if (t != null) {
				if (t.transform.GetComponent<Tile> () != null) {
					if (t.transform.GetComponent<Tile> ().isHouse) {
						speedMovimiento = normalSpeed;
						Node no = getNodoEnPos (transform.position);
						if (no != null) {
							currNode = no;
							dir = Vector2.up;
							targetNode = currNode.neighs [0];
							antNode = currNode;
							current = Mode.Chase;
							updateAnimacion ();
						}
					}
				}
			}
		}
	}

	void checkCollision(){
		
		Rect gRect = new Rect (transform.position, transform.GetComponent<SpriteRenderer> ().sprite.bounds.size / 4);
		Rect pRect = new Rect (pacman.transform.position, pacman.transform.GetComponent<SpriteRenderer> ().sprite.bounds.size / 4);

		if(gRect.Overlaps(pRect)){
			if (pacman.transform.GetComponent<PacManMovement> ().canBeEaten == true) {
				if (current == Mode.Scared) {
					comido ();
				} else {
					if (current != Mode.Comido) {
						GameObject.Find ("Juego").transform.GetComponent<Board> ().startDeath ();
					}
				}
			}
		}
	}

	void comido(){
		GameObject.Find ("Juego").GetComponent<Board> ().score += 200;
		current = Mode.Comido;
		previoSpeedMover = speedMovimiento;
		speedMovimiento = comidoSpeed;
		updateAnimacion ();
		GameObject.Find ("Juego").transform.GetComponent<Board> ().empezarComido (this.GetComponent<Ghost> ());
	}

	void updateAnimacion(){
		if (current != Mode.Scared && current != Mode.Comido) {
			if (current == Mode.Slow) {
				speedMovimiento = slowSpeed;
			} 
			if (dir == Vector2.up) {
				transform.GetComponent<Animator> ().runtimeAnimatorController = ghostArriba;
			} else if (dir == Vector2.down) {
				transform.GetComponent<Animator> ().runtimeAnimatorController = ghostAbajo;
			} else if (dir == Vector2.left) {
				transform.GetComponent<Animator> ().runtimeAnimatorController = ghostIzq;
			} else if (dir == Vector2.right) {
				transform.GetComponent<Animator> ().runtimeAnimatorController = ghostDer;
			} else {
				transform.GetComponent<Animator> ().runtimeAnimatorController = ghostIzq;
			}
		} else if (current == Mode.Scared) {
			transform.GetComponent<Animator> ().runtimeAnimatorController = ghostScared;

		} else if (current == Mode.Comido) {
			
			transform.GetComponent<Animator> ().runtimeAnimatorController = null;
			if (dir == Vector2.up) {
				transform.GetComponent<SpriteRenderer> ().sprite = ojosUp;
			} else if (dir == Vector2.down) {
				transform.GetComponent<SpriteRenderer> ().sprite = ojosAbajo;
			} else if (dir == Vector2.left) {
				transform.GetComponent<SpriteRenderer> ().sprite = ojosIzq;
			} else if (dir == Vector2.right) {
				transform.GetComponent<SpriteRenderer> ().sprite = ojosDer;
			} 
		} 


	}

	void updateModo(){
		if (current != Mode.Scared && current != Mode.Slow) {
			timerCambioMode += Time.deltaTime;
			if (iteradorCambioMode == 1) {
				if (current == Mode.Loco && timerCambioMode > locoGhostTimer1) {
					cambiarModo (Mode.Chase);
					timerCambioMode = 0;
				}
				if (current == Mode.Chase && timerCambioMode > chasingMode1) {
					iteradorCambioMode = 2;
					cambiarModo (Mode.Loco);
					timerCambioMode = 0;
				}
			} else if (iteradorCambioMode == 2) {

				if (current == Mode.Loco && timerCambioMode > locoGhostTimer2) {
					cambiarModo (Mode.Chase);
					timerCambioMode = 0;
				}
				if (current == Mode.Chase && timerCambioMode > chasingMode2) {
					iteradorCambioMode = 3;
					cambiarModo (Mode.Loco);
					timerCambioMode = 0;
				}

			} else if (iteradorCambioMode == 3) {
				if (current == Mode.Loco && timerCambioMode > locoGhostTimer3) {
					cambiarModo (Mode.Chase);
					timerCambioMode = 0;
				}
				if (current == Mode.Chase && timerCambioMode > chasingMode3) {
					iteradorCambioMode = 4;
					cambiarModo (Mode.Loco);
					timerCambioMode = 0;
				}

			} else if (iteradorCambioMode == 4) {

				if (current == Mode.Loco && timerCambioMode > locoGhostTimer4) {
					cambiarModo (Mode.Chase);
					timerCambioMode = 0;
				}
			}

		} else if (current == Mode.Scared) {
			
			scaredModoTimer += Time.deltaTime;
			if (scaredModoTimer >= scaredModoDuracion) {
				
				bgAudio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgNormalAudio;
				bgAudio.Play ();
				scaredModoTimer = 0;
				cambiarModo (anterior);
			}
			if (scaredModoTimer >= startWhiteAt) {
				
				whiteBlinkTimer += Time.deltaTime;

				if (whiteBlinkTimer >= 0.1f) {
					whiteBlinkTimer = 0;

					if (scaredModoEsBlanco) {
						transform.GetComponent<Animator> ().runtimeAnimatorController = ghostScared;
						scaredModoEsBlanco = false;

					} else {
						transform.GetComponent<Animator> ().runtimeAnimatorController = ghostBlanco;
						scaredModoEsBlanco = true;
					}
				}
			}
		} else if (current == Mode.Slow) {
			slowModeTimer += Time.deltaTime;
			if (slowModeTimer >= slowModeDuracion) {
				pacman.transform.GetComponent<PacManMovement> ().slowgText.enabled = false;
				//bgAudio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgNormalAudio;
				//bgAudio.Play ();
				slowModeTimer = 0;
				cambiarModo (anterior);
			}
			if (slowModeTimer >= startNormalAt) {
				normalBlinker += Time.deltaTime;

				if (normalBlinker >= 0.1f) {
					normalBlinker = 0;

					if (slowModeEsNormal) {
						speedMovimiento = slowSpeed;
						slowModeEsNormal = false;
					} else {
						speedMovimiento = normalSpeed;
						slowModeEsNormal = true;
					}
				}
			}
		}
	}

	void moverGhost(){

		if (targetNode != currNode && targetNode != null && !isInHouse) {

			if (over ()) {

				currNode = targetNode;
				transform.localPosition = currNode.transform.position;

				GameObject otroP = getPortal (currNode.transform.position);

				if (otroP != null) {
					transform.localPosition = otroP.transform.position;
					currNode = otroP.transform.GetComponent<Node> ();
				}

				targetNode = escogerSiguienteNodo ();
				antNode = currNode;
				currNode = null;
				updateAnimacion ();
			} else {

				transform.localPosition += (Vector3)dir * speedMovimiento * Time.deltaTime;
			}
		}
	}

	void cambiarModo(Mode m){

		if (current == Mode.Scared) {
			speedMovimiento = previoSpeedMover;
		} 
		if (m == Mode.Slow) {
			previoSpeedMover = speedMovimiento;
			speedMovimiento = slowSpeed;
		}
		if (m == Mode.Scared) {
			previoSpeedMover = speedMovimiento;
			speedMovimiento = scaredModoSpeed;
		}
		if (current != m) {
			anterior = current;
			current = m;
		}

		updateAnimacion ();
	}

	public void startScaredModo(){
		if (current != Mode.Comido) {
			scaredModoTimer = 0;
			bgAudio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgScaredAudio;
			bgAudio.Play ();
			cambiarModo (Mode.Scared);
		}
	}

	public void startSlowModo(){
		if (current != Mode.Comido) {
			slowModeTimer = 0;
			//bgAudio.clip = GameObject.Find ("Juego").transform.GetComponent<Board> ().bgScaredAudio;
			//bgAudio.Play ();
			cambiarModo (Mode.Slow);
		}
	}

	Vector2 getRedTargetTile(){
		Vector2 pacPos = pacman.transform.localPosition;
		Vector2 tt = new Vector2 (Mathf.RoundToInt (pacPos.x), Mathf.RoundToInt (pacPos.y));

		return tt;
	}

	Vector2 getPinkTargetTile(){

		Vector2 pacPos = pacman.transform.localPosition;
		Vector2 pacOr = pacman.GetComponent<PacManMovement> ().orient;

		Vector2 pmTile = new Vector2 (Mathf.RoundToInt (pacPos.x), Mathf.RoundToInt (pacPos.y));
		Vector2 tt = pmTile + (4 * pacOr);

		return tt;
		
	}

	Vector2 getBlueTargetTile(){
		Vector2 pacPos = pacman.transform.localPosition;
		Vector2 pacOr = pacman.GetComponent<PacManMovement> ().orient;

		Vector2 pmTile = new Vector2 (Mathf.RoundToInt (pacPos.x), Mathf.RoundToInt (pacPos.y));

		Vector2 tt = pmTile + (2 * pacOr);

		Vector2 tempRedPos = GameObject.Find ("RedGhost").transform.localPosition;

		tempRedPos = new Vector2 (Mathf.RoundToInt (tempRedPos.x), Mathf.RoundToInt (tempRedPos.y));
		float disRed = getDis (tempRedPos, tt);
		disRed *= 2;

		tt = new Vector2 (tempRedPos.x + disRed, tempRedPos.y + disRed);

		return tt;

	}

	Vector2 getOrangeTargetTile(){
		Vector2 pacPos = pacman.transform.localPosition;
		float distancia = getDis (transform.localPosition, pacPos);
		Vector2 tt = Vector2.zero;

		if (distancia > 8) {
			tt = new Vector2 (Mathf.RoundToInt (pacPos.x), Mathf.RoundToInt (pacPos.y));
			
		} else if (distancia < 8) {
			tt = homeNodo.transform.position;
		}
		return tt;
	}

	void releasePink(){
		if (tipoGhost == GhostTipo.Pink && isInHouse) {
			isInHouse = false;
		}
	}

	void releaseBlue(){
		if (tipoGhost == GhostTipo.Blue && isInHouse) {
			isInHouse = false;
		}
	}

	void releaseOrange(){
		if (tipoGhost == GhostTipo.Orange && isInHouse) {
			isInHouse = false;
		}
	}

	void releaseGhosts(){
		ghostReleaseTimer += Time.deltaTime;
		if (ghostReleaseTimer > pinkReleaseTimer) {
			releasePink ();
		}

		if (ghostReleaseTimer > blueReleaseTimer) {
			releaseBlue ();
		}

		if (ghostReleaseTimer > orangeReleaseTimer) {
			releaseOrange ();
		}

	}

	Vector2 getTargetTile(){
		Vector2 target = Vector2.zero;

		if (tipoGhost == GhostTipo.Red) {
			target = getRedTargetTile ();
		} else if (tipoGhost == GhostTipo.Pink) {
			target = getPinkTargetTile ();
		} else if (tipoGhost == GhostTipo.Blue) {
			target = getBlueTargetTile ();
		} else if (tipoGhost == GhostTipo.Orange) {
			target = getOrangeTargetTile ();
		}

		return target;
	}

	Vector2 getRandomTile(){
		int a = Random.Range (0, 26);
		int b = Random.Range (0, 32);
		return new Vector2 (a, b);
	}


	Node escogerSiguienteNodo(){
		Vector2 tarT = Vector2.zero;

		if (current == Mode.Chase) {
			tarT = getTargetTile ();
		} else if (current == Mode.Loco) {
			tarT = homeNodo.transform.position;
		} else if (current == Mode.Scared) {
			tarT = getRandomTile ();
		} else if (current == Mode.Comido) {
			tarT = house.transform.position;
		} 
			
		Node moverANode = null;

		Node[] nodesEnc = new Node[4];
		Vector2[] foundNodesDir = new Vector2[4];
		int contNodo = 0;

		for (int i = 0; i < currNode.neighs.Length; i++) {
			if (currNode.dirValidas [i] != dir * -1) {
				if (current != Mode.Comido) {
					GameObject t = getTileEnPos (currNode.transform.position);
					if (t.transform.GetComponent<Tile> ().isEntradaHouse == true && t != null) {
						if (currNode.dirValidas [i] != Vector2.down) {
							nodesEnc [contNodo] = currNode.neighs [i];
							foundNodesDir [contNodo] = currNode.dirValidas [i];
							contNodo++;
						}
					} else {
						nodesEnc [contNodo] = currNode.neighs [i];
						foundNodesDir [contNodo] = currNode.dirValidas [i];
						contNodo++;
					}
				} else {
					nodesEnc [contNodo] = currNode.neighs [i];
					foundNodesDir [contNodo] = currNode.dirValidas [i];
					contNodo++;
				}

			}
		}

		if (nodesEnc.Length == 1) {
			moverANode = nodesEnc [0];
			dir = foundNodesDir [0];
		}

		if (nodesEnc.Length > 1) {
			float disMenor = 100000f;
			for (int i = 0; i < nodesEnc.Length; i++) {
				if (foundNodesDir [i] != Vector2.zero) {
					float dist = getDis (nodesEnc [i].transform.position, tarT);

					if (dist < disMenor) {
						disMenor = dist;
						moverANode = nodesEnc [i];
						dir = foundNodesDir [i];
					}
				}
			}
		}
		return moverANode;
	}


	Node getNodoEnPos(Vector2 pos){

		GameObject t = GameObject.Find ("Juego").GetComponent<Board> ().board [(int)pos.x, (int)pos.y];
		if (t != null) {

			return t.GetComponent<Node>();
		}
		return null;
	}

	GameObject getTileEnPos(Vector2 pos){
		GameObject t = GameObject.Find ("Juego").transform.GetComponent<Board> ().board [(int)pos.x, (int)pos.y];
		if (t != null) {
			return t;
		}
		return null;
	}

	GameObject getPortal (Vector2 pos){
		GameObject t = GameObject.Find ("Juego").GetComponent<Board> ().board[(int)pos.x, (int)pos.y];
		if (t != null) {
			if (t.GetComponent<Tile> () != null) {
				if (t.GetComponent<Tile>().isPortal) {
					GameObject otherP = t.GetComponent<Tile> ().portalReciever;
					return otherP;
				}
			}
		}
		return null;
	}

	float lenFromNode (Vector2 tp){
		Vector2 vec = tp - (Vector2)antNode.transform.position;
		return vec.sqrMagnitude;
	}

	bool over(){
		float nodeAT = lenFromNode (targetNode.transform.position);
		float nodeMismo = lenFromNode (transform.localPosition);

		return nodeMismo > nodeAT;
	}

	float getDis(Vector2 a,Vector2 b){
		float dx = a.x - b.x;
		float dy = a.y - b.y;

		float distancia = Mathf.Sqrt (dx * dx + dy * dy);

		return distancia;
	}



}
